----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/30/2020 01:39:04 PM
-- Design Name: 
-- Module Name: axi4_spi_master_core - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity axi4_spi_master_core is
    generic (
                C_S_AXI_DATA_WIDTH	: integer	:= 32;
                C_S_AXI_ADDR_WIDTH	: integer	:= 5;
                -- spi generics
                DATA_WIDTH      : natural                       := 24;   -- must be multiple of 8(A Byte)
                MODE            : std_logic_vector(1 downto 0)  := "00"; -- 00 => (CPOL = 0 , CPHA = 0), 01 => (CPOL = 0 , CPHA = 1), 10 => (CPOL = 1 , CPHA = 0), 11 => (CPOL = 1 , CPHA = 1)
                SCLK_PERIOD     : natural                       := 40;   -- SCLk period in ns and must be multiple of 20 (1/100MHz)*2
                T_DELAY         : natural                       := 50;   -- CS falling edge to clock edge and must be multiple of 10 (1/100MHz) min = 40 ns
                T_BYTE          : natural                       := 130;  -- LSB last byte to MSB next byte in ns and must be multiple of 10(1/100MHz)
                T_QUIET         : natural                       := 50;   -- Last SCLK edge to SELN rising edge in ns and must be multiple of 10(1/100MHz)
                T_CS            : natural                       := 50;   -- SPI idle time seln rising to falling edge in ns and must be multiple of 10(1/100MHz)
                OP_MODE_WIDTH   : natural                       := 2;    -- The width of the oparation bit/bits to indicate read and write operations (the MSB bit/bits)
                ADDRESS_BTYTES  : natural                       := 2     -- How many bytes belong to the address; this is important for read operation   
                
            );
    port    (
                -- spi interface
                sclk             : out STD_LOGIC;
                seln             : out STD_LOGIC;
                mosi             : out STD_LOGIC;
                miso             : in  STD_LOGIC;
                -- axi4 interface
                S_AXI_ACLK	      : in std_logic; -- 100 Mhz
                S_AXI_ARESETN     : in std_logic; -- active low
                S_AXI_AWADDR      : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
                S_AXI_AWPROT      : in std_logic_vector(2 downto 0);
                S_AXI_AWVALID     : in std_logic;
                S_AXI_AWREADY     : out std_logic;
                S_AXI_WDATA       : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
                S_AXI_WSTRB       : in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
                S_AXI_WVALID      : in std_logic;
                S_AXI_WREADY      : out std_logic;
                S_AXI_BRESP	      : out std_logic_vector(1 downto 0);
                S_AXI_BVALID      : out std_logic;
                S_AXI_BREADY      : in std_logic;
                S_AXI_ARADDR      : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
                S_AXI_ARPROT      : in std_logic_vector(2 downto 0);
                S_AXI_ARVALID     : in std_logic;
                S_AXI_ARREADY     : out std_logic;
                S_AXI_RDATA	      : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
                S_AXI_RRESP	      : out std_logic_vector(1 downto 0);
                S_AXI_RVALID      : out std_logic;
                S_AXI_RREADY      : in std_logic
            );
end axi4_spi_master_core;

architecture Behavioral of axi4_spi_master_core is



    component spi_master_core is
        Generic (
                    DATA_WIDTH      : natural                       := 24;   -- must be multiple of 8(A Byte)
                    MODE            : std_logic_vector(1 downto 0)  := "00"; -- 00 => (CPOL = 0 , CPHA = 0), 01 => (CPOL = 0 , CPHA = 1), 10 => (CPOL = 1 , CPHA = 0), 11 => (CPOL = 1 , CPHA = 1)
                    SCLK_PERIOD     : natural                       := 40;   -- SCLk period in ns and must be multiple of 20 (1/100MHz)*2
                    T_DELAY         : natural                       := 50;   -- CS falling edge to clock edge and must be multiple of 10 (1/100MHz) min = 40 ns
                    T_BYTE          : natural                       := 130;  -- LSB last byte to MSB next byte in ns and must be multiple of 10(1/100MHz)
                    T_QUIET         : natural                       := 50;   -- Last SCLK edge to SELN rising edge in ns and must be multiple of 10(1/100MHz)
                    T_CS            : natural                       := 50;   -- SPI idle time seln rising to falling edge in ns and must be multiple of 10(1/100MHz)
                    OP_MODE_WIDTH   : natural                       := 2;    -- The width of the oparation bit/bits to indicate read and write operations (the MSB bit/bits)
                    ADDRESS_BTYTES  : natural                       := 2     -- How many bytes belong to the address; this is important for read operation     
             );
             
        Port  (
                   clk_100          : in STD_LOGIC;
                   rst              : in STD_LOGIC := '1';
                   read_mode_code   : in STD_LOGIC_VECTOR(OP_MODE_WIDTH - 1 downto 0);  
                   --read fifo interface (to read data and send it to slave)
                   fifo_in_data     : in STD_LOGIC_VECTOR (DATA_WIDTH - 1 downto 0) := x"ADBEEF";
                   fifo_in_empty    : in STD_LOGIC := '0';
                   fifo_in_rd_en    : out STD_LOGIC;
                   --write fifo interface (to write data arrived from salve)
                   fifo_out_data    : out STD_LOGIC_VECTOR (7 downto 0);
                   fifo_out_wr_en   : out STD_LOGIC;
                   --spi interface
                   sclk             : out STD_LOGIC;
                   seln             : out STD_LOGIC;
                   mosi             : out STD_LOGIC;
                   miso             : in  STD_LOGIC;
                   core_version     : out STD_LOGIC_VECTOR(31 downto 0)
         );
    end component spi_master_core;




    component spi_master_core_axi4_interface is
        generic (
                    C_S_AXI_DATA_WIDTH	: integer	:= 32;
                    C_S_AXI_ADDR_WIDTH	: integer	:= 5
                );
        port    (
                -- Users to add ports here
                
                    core_version      : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
                    read_mode_code    : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
                    tx_data	          : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
                    tx_ready          : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
                    tx_data_valid     : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
                    rx_data	          : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
            
                    S_AXI_ACLK	      : in std_logic; -- 100 Mhz
                    S_AXI_ARESETN     : in std_logic; -- active low
                    S_AXI_AWADDR      : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
                    S_AXI_AWPROT      : in std_logic_vector(2 downto 0);
                    S_AXI_AWVALID     : in std_logic;
                    S_AXI_AWREADY     : out std_logic;
                    S_AXI_WDATA       : in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
                    S_AXI_WSTRB       : in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
                    S_AXI_WVALID      : in std_logic;
                    S_AXI_WREADY      : out std_logic;
                    S_AXI_BRESP	      : out std_logic_vector(1 downto 0);
                    S_AXI_BVALID      : out std_logic;
                    S_AXI_BREADY      : in std_logic;
                    S_AXI_ARADDR      : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
                    S_AXI_ARPROT      : in std_logic_vector(2 downto 0);
                    S_AXI_ARVALID     : in std_logic;
                    S_AXI_ARREADY     : out std_logic;
                    S_AXI_RDATA	      : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
                    S_AXI_RRESP	      : out std_logic_vector(1 downto 0);
                    S_AXI_RVALID      : out std_logic;
                    S_AXI_RREADY      : in std_logic
                );
    end component spi_master_core_axi4_interface;


    -- nets
    signal spi_reset_net        : std_logic;
    signal core_version_net     : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);   
    signal read_mode_code_net   : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal tx_data_net	        : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal tx_ready_net         : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal tx_data_valid_net    : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    signal rx_data_net	        : std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    
    signal empty  : std_logic := '1';
    signal tx_data_valid_pulse  : std_logic := '0';
    signal tx_data_valid_pipe_1 : std_logic := '0';

begin



    spi_reset_net   <= not S_AXI_ARESETN;
    
generate_tx_data_valid_pulse_proc:
    process(S_AXI_ACLK,spi_reset_net)
    begin
        if rising_edge(S_AXI_ACLK) then 
            
            tx_data_valid_pulse     <= '0';
            tx_data_valid_pipe_1    <= tx_data_valid_net(0);
            
            if tx_data_valid_pipe_1 = '0' and tx_data_valid_net(0) = '1' then
                tx_data_valid_pulse     <= '1';
            end if; 
            
            if tx_data_valid_pulse = '1' then
                empty   <= '0';
            end if;
            
            if tx_ready_net(0) = '1' then
                empty   <= '1';
            end if;
        
            if spi_reset_net = '1' then
                
                tx_data_valid_pulse     <= '0';
                tx_data_valid_pipe_1    <= '0';
                empty   <= '1';
                
            end if;
        
        end if;
    
    end process generate_tx_data_valid_pulse_proc; 

spi_master_core_inst: spi_master_core
        Generic map (
                        DATA_WIDTH      => DATA_WIDTH    ,
                        MODE            => MODE          ,
                        SCLK_PERIOD     => SCLK_PERIOD   ,
                        T_DELAY         => T_DELAY       ,
                        T_BYTE          => T_BYTE        ,
                        T_QUIET         => T_QUIET       ,
                        T_CS            => T_CS          ,
                        OP_MODE_WIDTH   => OP_MODE_WIDTH ,
                        ADDRESS_BTYTES  => ADDRESS_BTYTES
                    )
             
        Port map  (
                       clk_100          => S_AXI_ACLK,
                       rst              => spi_reset_net,
                       read_mode_code   => read_mode_code_net(OP_MODE_WIDTH - 1 downto 0),
                       fifo_in_data     => tx_data_net(DATA_WIDTH - 1 downto 0),     
                       fifo_in_empty    => empty,    
                       fifo_in_rd_en    => tx_ready_net(0), 
                       fifo_out_data    => rx_data_net(7 downto 0),	     
                       fifo_out_wr_en   => open,
                       sclk             => sclk,
                       seln             => seln,
                       mosi             => mosi,
                       miso             => miso,
                       core_version     => core_version_net
         );
         
spi_master_reg_map_inst: spi_master_core_axi4_interface
        generic map (
                        C_S_AXI_DATA_WIDTH	=> C_S_AXI_DATA_WIDTH,
                        C_S_AXI_ADDR_WIDTH	=> C_S_AXI_ADDR_WIDTH
                    )
                    
        port map   (
                
                        core_version      => core_version_net,  
                        read_mode_code    => read_mode_code_net,
                        tx_data	          => tx_data_net,	      
                        tx_ready          => tx_ready_net,      
                        tx_data_valid     => tx_data_valid_net, 
                        rx_data	          => rx_data_net,	      
                
                        S_AXI_ACLK	      => S_AXI_ACLK	  ,
                        S_AXI_ARESETN     => S_AXI_ARESETN,
                        S_AXI_AWADDR      => S_AXI_AWADDR ,
                        S_AXI_AWPROT      => S_AXI_AWPROT ,
                        S_AXI_AWVALID     => S_AXI_AWVALID,
                        S_AXI_AWREADY     => S_AXI_AWREADY,
                        S_AXI_WDATA       => S_AXI_WDATA  ,
                        S_AXI_WSTRB       => S_AXI_WSTRB  ,
                        S_AXI_WVALID      => S_AXI_WVALID ,
                        S_AXI_WREADY      => S_AXI_WREADY ,
                        S_AXI_BRESP	      => S_AXI_BRESP	, 
                        S_AXI_BVALID      => S_AXI_BVALID ,
                        S_AXI_BREADY      => S_AXI_BREADY ,
                        S_AXI_ARADDR      => S_AXI_ARADDR ,
                        S_AXI_ARPROT      => S_AXI_ARPROT ,
                        S_AXI_ARVALID     => S_AXI_ARVALID,
                        S_AXI_ARREADY     => S_AXI_ARREADY,
                        S_AXI_RDATA	      => S_AXI_RDATA	, 
                        S_AXI_RRESP	      => S_AXI_RRESP	, 
                        S_AXI_RVALID      => S_AXI_RVALID ,
                        S_AXI_RREADY      => S_AXI_RREADY 
                );

    

end Behavioral;
